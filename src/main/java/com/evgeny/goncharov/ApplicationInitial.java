package com.evgeny.goncharov;

import com.evgeny.goncharov.config.WebConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

//наш класс котором мы разворачиваем настройку сервлетов
public class ApplicationInitial implements WebApplicationInitializer {



    private static final String DISPATCHER = "dispatcher";

    //данный метод говорит что мы хотим выполнить какие то действия перед тем
    //как будет разворачиваться наш контекст
    //сервлет это штука которая позволяет делать http
    //Интерфейс javax.servlet.ServletConfig используется
    // для передачи конфигурационной информации сервлету.
    public void onStartup(ServletContext servletContext) throws ServletException {
        //создать корневой контекст приложения Spring
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.register(WebConfig.class);

        //мы добавляем листенер
        servletContext.addListener(new ContextLoaderListener(ctx));

        //в диспечер сервлетов добавим servletContext
        ServletRegistration.Dynamic servlet = servletContext
                .addServlet(DISPATCHER, new DispatcherServlet(ctx));

        //теперь в наш сервлет добавляем мапинг
        //мы указываем по какому контекст руту будет доступно наше приложение
        servlet.addMapping("/");

    }


}
