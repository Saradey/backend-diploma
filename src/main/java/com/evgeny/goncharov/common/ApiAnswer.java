package com.evgeny.goncharov.common;

public class ApiAnswer {

    public static final String OK = "Ok";

    public static final String LOGIN_ALREADY_EXISTS = "Such login already exists";

    public static final String ERROR_API_KEY = "Error api key";

    public static final String ERROR_PASSWORD_OR_LOGIN = "Error password";

    public static final String ERROR_TOKEN = "Error token";

    public static final String DONE_ARTICLE = "Article save";

    public static final String DONE_DELETE_ARTICLE = "Article delete";

    public static final String ERROR_DELETE_ARTICLE = "Article delete error from db";

}