package com.evgeny.goncharov.common;

public class ApiMethod {


    //UserController
    public static final String ROOT_USER_CONTROLLER = "/user";

    public static final String REGISTRATION_USER = "/registration";

    public static final String AUTHENTICATION = "/authentication";

    public static final String LOGOUT = "/logout";


    //ArticleController

    public static final String ARTICLE = "/article";

    public static final String CREATE_ARTICLE = "/create";

    public static final String DELETE_ARTICLE = "/delete";

    public static final String GET_LAST_ARTICLE = "/getLastArticle";

    public static final String GET_ARTICLE_OFFSET = "/getArticleOffset";

}