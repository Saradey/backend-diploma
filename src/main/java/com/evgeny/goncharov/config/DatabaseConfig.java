package com.evgeny.goncharov.config;


import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.util.Properties;

//нужно пометить этот класс как Configuration
//для того что бы указать что он DriverManager.deregisterDriver(driver); является конфигурацией
@Configuration
@EnableJpaRepositories("com.evgeny.goncharov.repository") //этой анотацией мы говорим spring context что мы будем
// использовать JPA а именно Spring data который построен на паттерне репозитори
//и в value мы указываем в каком именно пакете у=будут лежать наши репозитории
@EnableTransactionManagement //операции с базой данных
@ComponentScan("com.evgeny.goncharov") //рут пакет где искать все репозитории конфиги и тому подобное
@PropertySource("classpath:db.properties") //что бы наша база данных занала какой файл использовать
public class DatabaseConfig {

    //здесь мы должны заинджектить с помощью этой анотации
    //объект invariant
    @Resource
    private Environment env; //с его помощью мы сможем получить доступ к properties файлам
    //и получать с данного property файла какие то значения


    //метод который вовзращает datasource для того что бы сконфигурировать базу данных
    //именно он будет связующим звеном между базой данных и спринг фраемворков
    //спринг имеет такую возможность как DI
    //иницилизация бинов без участия разработчика
    @Bean
    public DataSource getDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(env.getRequiredProperty("db.url"));
        ds.setDriverClassName(env.getRequiredProperty("db.driver"));
        ds.setUsername(env.getRequiredProperty("db.username"));
        ds.setPassword(env.getRequiredProperty("db.password"));

        ds.setInitialSize(Integer.valueOf(env.getRequiredProperty("db.initialSize")));
        ds.setMinIdle(Integer.valueOf(env.getRequiredProperty("db.minIdle")));
        ds.setMaxIdle(Integer.valueOf(env.getRequiredProperty("db.maxIdle")));
        ds.setTimeBetweenEvictionRunsMillis(Long.valueOf(env.getRequiredProperty("db.timeBetweenEvictionRunsMillis")));
        ds.setMinEvictableIdleTimeMillis(Long.valueOf(env.getRequiredProperty("db.minEvictableIdleTimeMillis")));
        ds.setTestOnBorrow(Boolean.valueOf(env.getRequiredProperty("db.testOnBorrow")));
        ds.setValidationQuery(env.getRequiredProperty("db.validationQuery"));


        return ds;
    }


    //
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
        //апачевский датабейс конекшен пул
        LocalContainerEntityManagerFactoryBean lf = new LocalContainerEntityManagerFactoryBean();
        lf.setDataSource(getDataSource());
        //он будет сканить именно те entity которыми он будет управлять
        lf.setPackagesToScan(env.getRequiredProperty("db.entity.package"));

        //мы говорим что в качестве jpa провайдера мы будем использовать Hibernate
        lf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        lf.setJpaProperties(getHibernateProperties());
        return lf;
    }

    //это из ного Property файла
    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("hibernate.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }


    //
    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory().getObject());
        return manager;
    }

}


//DAO это классы которые дают доступ к данным, в определнном случае это доступ к
//базе данных

//транзацкии это предоставление возможности rallback


//коннекшен пул
//это возможность проекта своемренно и на перед создавать готовые коннекшены
//что бы отдавать их пользователям и сессиям