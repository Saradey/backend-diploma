package com.evgeny.goncharov.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Collections;
import java.util.List;

//нужно сконфигурировать web контекст
@Configuration  //данная антотация говорит что нужно ее выполнить перед тем как
//разворачивать контект спринга
@EnableWebMvc   //даст нам возможность использовать res контроллеров
@ComponentScan("com.evgeny.goncharov")  //говорит о том где мы должны искать все наши бины
public class WebConfig extends WebMvcConfigurerAdapter {


    //для преобразование из класса в json ответ
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter mapping = new MappingJackson2HttpMessageConverter();
        mapping.setObjectMapper(new ObjectMapper());
        mapping.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        converters.add(mapping);
    }




}
