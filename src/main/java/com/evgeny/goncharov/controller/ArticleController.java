package com.evgeny.goncharov.controller;

import com.evgeny.goncharov.common.ApiAnswer;
import com.evgeny.goncharov.common.ApiMethod;
import com.evgeny.goncharov.entitiesResponse.Answer;
import com.evgeny.goncharov.entitiesResponse.any.AnyResponse;
import com.evgeny.goncharov.entitiesResponse.ok.ArticleResponse;
import com.evgeny.goncharov.security.ManagerSecurity;
import com.evgeny.goncharov.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiMethod.ARTICLE)
public class ArticleController extends PrimordialController {

    @Autowired
    private ManagerSecurity managerSecurity;

    @Autowired
    private ArticleService articleService;


    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    //продакшен
    //запросы
    //
    //опубликовать статью: api key, id пользователя, текст статьи, токен
    //загрузить статьи пользователя: сертификат, id пользователя, токен
    //загрузить статьи в порядке публикации: сертификат, ?!?!
    //загрузить статьи по классификации: сертификат, id классификации
    //удалить статью: сертификат, токен, логин, id статьи

    //http://localhost:8080/article/create?apiKey=2f1b2391fe4f1db4e185c100cc370a90&login=123&token=eT8W232p6APKn21PaBHLgU3xmoNBkwLjS82&text=HeSheItWhyVoidDoomMoscow
    @RequestMapping(value = ApiMethod.CREATE_ARTICLE, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Answer> createArticle(@RequestParam("apiKey") String apiKey,
                                                @RequestParam("login") String login,
                                                @RequestParam("token") String token,
                                                @RequestParam("text") String text) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (managerSecurity.verificationToken(login, token)) {
                //запилить бизнес логику
                articleService.saveArticle(login, text);

                return ResponseEntity.ok(new Answer(new AnyResponse(ApiAnswer.DONE_ARTICLE)));
            } else return createError(new AnyResponse(ApiAnswer.ERROR_TOKEN), HttpStatus.UNAUTHORIZED);
        } else return createError(new AnyResponse(ApiAnswer.ERROR_API_KEY), HttpStatus.UNAUTHORIZED);
    }


    //http://localhost:8080/article/delete?apiKey=2f1b2391fe4f1db4e185c100cc370a90&login=123&token=t1xIA00JsA4k0qL216r7b9YqqJI65Wvj3LQ&idArticle=1
    @RequestMapping(value = ApiMethod.DELETE_ARTICLE, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Answer> deleteArticle(@RequestParam("apiKey") String apiKey,
                                                @RequestParam("login") String login,
                                                @RequestParam("token") String token,
                                                @RequestParam("idArticle") Long idArticle) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (managerSecurity.verificationToken(login, token)) {
                if (articleService.deleteArticle(idArticle)) {
                    return ResponseEntity.ok(new Answer(new AnyResponse(ApiAnswer.DONE_DELETE_ARTICLE)));
                } else
                    return createError(new AnyResponse(ApiAnswer.ERROR_DELETE_ARTICLE), HttpStatus.INTERNAL_SERVER_ERROR);
            } else return createError(new AnyResponse(ApiAnswer.ERROR_TOKEN), HttpStatus.UNAUTHORIZED);
        } else return createError(new AnyResponse(ApiAnswer.ERROR_API_KEY), HttpStatus.UNAUTHORIZED);
    }


    //http://localhost:8080/article/getLastArticle?apiKey=2f1b2391fe4f1db4e185c100cc370a90&login=123&token=6q0901rC4S8na4ma7bah4aT80u58Bp3iUzf
    @RequestMapping(value = ApiMethod.GET_LAST_ARTICLE, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Answer> getLastArticle(@RequestParam("apiKey") String apiKey,
                                                 @RequestParam("login") String login,
                                                 @RequestParam("token") String token) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (managerSecurity.verificationToken(login, token)) {
                return ResponseEntity.ok(new Answer(new ArticleResponse(articleService.getLastArticle())));
            } else return createError(new AnyResponse(ApiAnswer.ERROR_TOKEN), HttpStatus.UNAUTHORIZED);
        } else return createError(new AnyResponse(ApiAnswer.ERROR_API_KEY), HttpStatus.UNAUTHORIZED);
    }

    //http://localhost:8080/article/getArticleOffset?apiKey=2f1b2391fe4f1db4e185c100cc370a90&login=123&token=6q0901rC4S8na4ma7bah4aT80u58Bp3iUzf&offset=10
    @RequestMapping(value = ApiMethod.GET_ARTICLE_OFFSET, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Answer> getArticleOffset(@RequestParam("apiKey") String apiKey,
                                                   @RequestParam("login") String login,
                                                   @RequestParam("token") String token,
                                                   @RequestParam("offset") long idOffset) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (managerSecurity.verificationToken(login, token)) {
                return ResponseEntity.ok(new Answer(new ArticleResponse(articleService.getArticleOffset(idOffset))));
            } else return createError(new AnyResponse(ApiAnswer.ERROR_TOKEN), HttpStatus.UNAUTHORIZED);
        } else return createError(new AnyResponse(ApiAnswer.ERROR_API_KEY), HttpStatus.UNAUTHORIZED);
    }


}
