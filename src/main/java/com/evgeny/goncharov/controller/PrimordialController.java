package com.evgeny.goncharov.controller;


import com.evgeny.goncharov.entitiesResponse.Answer;
import com.evgeny.goncharov.entitiesResponse.Response;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class PrimordialController {

    protected ResponseEntity<Answer> createError(Response response, HttpStatus httpStatus) {
        return new ResponseEntity<>(new Answer(response), new HttpHeaders(), httpStatus);
    }

}
