package com.evgeny.goncharov.controller;

import com.evgeny.goncharov.common.ApiAnswer;
import com.evgeny.goncharov.common.ApiMethod;
import com.evgeny.goncharov.entitiesResponse.Answer;
import com.evgeny.goncharov.entitiesResponse.any.AnyResponse;
import com.evgeny.goncharov.entitiesResponse.ok.MainTokenResponse;
import com.evgeny.goncharov.security.ManagerSecurity;
import com.evgeny.goncharov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiMethod.ROOT_USER_CONTROLLER)
public class UserController extends PrimordialController{

    @Autowired
    private UserService userService;

    @Autowired
    private ManagerSecurity managerSecurity;


    //http://localhost:8080/user/test
//    @RequestMapping(value = "/test", method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseEntity<Answer> registrationUser() {
//        HttpHeaders responseHeaders = new HttpHeaders();
////        Response response = new UserRegOK();
////        return new ResponseEntity<>(new Answer(response), responseHeaders, HttpStatus.LOGIN_ALREADY_EXISTS);
////        return ResponseEntity.ok(new Answer(response));
//    }


    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    //продакшен
    //запросы
    //
    //регистрация, вид: сертификат, пароль, логин, почта
    //авторизация, вид: сертификат, пароль, логин           - ответ временный токен
    //разлогиниться

    //Ответ 409 Conflict («конфликт») уже есть такой login или email
    //200 OK («хорошо») регистрация прошла
    //401 Unauthorized не подходит токен или api key
    //http://localhost:8080/user/registration?password=123&login=123&mail=123&apiKey=2f1b2391fe4f1db4e185c100cc370a90
    @RequestMapping(value = ApiMethod.REGISTRATION_USER, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Answer> registrationUser(@RequestParam("password") String pas,
                                                   @RequestParam("login") String log,
                                                   @RequestParam("mail") String mail,
                                                   @RequestParam("apiKey") String apiKey) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (userService.saveAndFlush(pas, log, mail))
                return ResponseEntity.ok(new Answer(new AnyResponse(ApiAnswer.OK)));
            else return createError(new AnyResponse(ApiAnswer.LOGIN_ALREADY_EXISTS), HttpStatus.CONFLICT);
        } else return createError(new AnyResponse(ApiAnswer.ERROR_API_KEY), HttpStatus.UNAUTHORIZED);
    }


    //http://localhost:8080/user/authentication?login=123&password=123&apiKey=2f1b2391fe4f1db4e185c100cc370a90
    @RequestMapping(value = ApiMethod.AUTHENTICATION, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Answer> authentication(@RequestParam("login") String log,
                                                 @RequestParam("password") String pas,
                                                 @RequestParam("apiKey") String apiKey) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (userService.checkLoginPassword(log, pas))
                return ResponseEntity.ok(new Answer(new MainTokenResponse(managerSecurity.generateToken(log))));
            else return createError(new AnyResponse(ApiAnswer.ERROR_PASSWORD_OR_LOGIN), HttpStatus.UNAUTHORIZED);
        } else return createError(new AnyResponse(ApiAnswer.ERROR_API_KEY), HttpStatus.UNAUTHORIZED);
    }



    //http://localhost:8080/user/logout?token=6K3mR4VV4bFeeSh4tK064U81Bl3hVs0MH63&apiKey=2f1b2391fe4f1db4e185c100cc370a90&login=123
    @RequestMapping(value = ApiMethod.LOGOUT, method = RequestMethod.GET)
    public void logout(@RequestParam("token") String token,
                       @RequestParam("apiKey") String apiKey,
                       @RequestParam("login") String login) {
        if (managerSecurity.verificationApiKey(apiKey)) {
            if (managerSecurity.verificationToken(login, token))
                managerSecurity.logout(login, token);
        }
    }



}
