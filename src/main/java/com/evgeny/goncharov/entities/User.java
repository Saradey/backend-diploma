package com.evgeny.goncharov.entities;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user_user")
public class User {



    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Id
    @Column(name = "login", nullable = false, length = 50)
    private String login;

    @Column(name = "mail", nullable = false, length = 50)
    private String mail;



    public User() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
