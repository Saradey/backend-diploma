package com.evgeny.goncharov.entitiesResponse;

public class Answer {

    private Response response;

    public Answer(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}