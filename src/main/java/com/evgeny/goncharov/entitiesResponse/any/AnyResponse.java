package com.evgeny.goncharov.entitiesResponse.any;

import com.evgeny.goncharov.entitiesResponse.Response;

public class AnyResponse implements Response {

    private String code;

    public AnyResponse(String registration) {
        this.code = registration;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
