package com.evgeny.goncharov.entitiesResponse.ok;

import com.evgeny.goncharov.entities.Article;
import com.evgeny.goncharov.entitiesResponse.Response;

import java.util.List;

public class ArticleResponse implements Response {

    private List<Article> list;

    public ArticleResponse(List<Article> list) {
        this.list = list;
    }

    public List<Article> getList() {
        return list;
    }

    public void setList(List<Article> list) {
        this.list = list;
    }
}