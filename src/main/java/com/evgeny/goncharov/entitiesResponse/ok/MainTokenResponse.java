package com.evgeny.goncharov.entitiesResponse.ok;

import com.evgeny.goncharov.entitiesResponse.Response;

public class MainTokenResponse implements Response {

    private String token;


    public MainTokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}