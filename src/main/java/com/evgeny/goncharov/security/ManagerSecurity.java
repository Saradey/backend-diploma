package com.evgeny.goncharov.security;

public interface ManagerSecurity {


    Boolean verificationApiKey(String string);

    String generateToken(String login);

    Boolean verificationToken(String login, String token);

    void logout(String login, String token);

}
