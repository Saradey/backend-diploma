package com.evgeny.goncharov.security;

import com.evgeny.goncharov.common.ApiConst;
import com.evgeny.goncharov.security.generateToken.ManagerGenerateToken;
import com.evgeny.goncharov.security.tokenPool.TokenPools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerSecurityImpl implements ManagerSecurity{

    @Autowired
    private ManagerGenerateToken managerGenerateToken;

    @Autowired
    private TokenPools tokenPools;


    @Override
    public Boolean verificationApiKey(String string) {
        return ApiConst.API_KEY.equals(string);
    }

    @Override
    public String generateToken(String login) {
        return managerGenerateToken.generateToken(login);
    }

    @Override
    public Boolean verificationToken(String login, String token) {
        return tokenPools.verificationToken(login, token);
    }

    @Override
    public void logout(String login, String token) {
        tokenPools.logout(login, token);
    }
}
