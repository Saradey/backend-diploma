package com.evgeny.goncharov.security.generateToken;

public interface ManagerGenerateToken {

    String generateToken(String login);

}
