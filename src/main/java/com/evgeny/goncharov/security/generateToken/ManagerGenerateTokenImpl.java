package com.evgeny.goncharov.security.generateToken;

import com.evgeny.goncharov.security.tokenPool.TokenPools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerGenerateTokenImpl implements ManagerGenerateToken{


    @Autowired
    private TokenPools tokenPools;


    private static final int LENGTH_TOKEN = 35;

    private static final int MIN_NUMBER = 48;

    private static final int MAX_NUMBER = 57;

    private static final int MIN_UPPERCASE = 65;

    private static final int MAX_UPPERCASE = 90;

    private static final int MIN_CHARACTER = 97;

    private static final int MAX_CHARACTER = 122;


    @Override
    public String generateToken(String login) {
        String token = generate();
        tokenPools.insertToken(login, token);
        return token;
    }


    private String generate(){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < LENGTH_TOKEN; i++){
            switch (generate(1, 3)){
                case 1:
                    stringBuilder.append((char)generate(MIN_NUMBER, MAX_NUMBER));
                    break;

                case 2:
                    stringBuilder.append((char)generate(MIN_UPPERCASE, MAX_UPPERCASE));
                    break;

                case 3:
                    stringBuilder.append((char)generate(MIN_CHARACTER, MAX_CHARACTER));
                    break;
            }
        }
        return stringBuilder.toString();
    }



    private static int generate(int min, int max) {
        max -= min;
        int value = (int) (Math.random() * ++max) + min;
        return value;
    }


}