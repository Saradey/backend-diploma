package com.evgeny.goncharov.security.tokenPool;

public interface TokenPools {

    void insertToken(String login, String token);

    Boolean verificationToken(String login, String token);

    void logout(String login, String token);

}
