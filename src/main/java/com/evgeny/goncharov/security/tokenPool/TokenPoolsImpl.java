package com.evgeny.goncharov.security.tokenPool;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeMap;

@Service
public class TokenPoolsImpl implements TokenPools {

    private Map<String, String> pool = new TreeMap();


    @Override
    public void insertToken(String login, String token) {
        System.out.println(token + " " + login);
        pool.put(login, token);
        System.out.println(pool);
    }

    @Override
    public Boolean verificationToken(String login, String token) {
        if (pool.containsKey(login))
            return pool.containsValue(token);
        else return false;
    }

    @Override
    public void logout(String login, String token) {
        System.out.println(pool.remove(login, token));
        System.out.println(pool);
    }

}