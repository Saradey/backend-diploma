package com.evgeny.goncharov.services;

import com.evgeny.goncharov.entities.Article;

import java.util.List;

public interface ArticleService {


    void saveArticle(String login, String text);

    boolean deleteArticle(long idArticle);

    List<Article> getLastArticle();

    List<Article> getArticleOffset(long idOffset);

}
