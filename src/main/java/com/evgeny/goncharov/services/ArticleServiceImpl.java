package com.evgeny.goncharov.services;

import com.evgeny.goncharov.entities.Article;
import com.evgeny.goncharov.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    private static final int SIZE_LIMIT = 10;

    @Override
    public void saveArticle(String login, String text) {
        Article article = new Article();
        article.setId_user(login);
        article.setText(text);
        articleRepository.saveAndFlush(article);
    }


    @Override
    public boolean deleteArticle(long idArticle) {
        if (articleRepository.existsById(idArticle)){
            articleRepository.deleteById(idArticle);
            return true;
        } else return false;
    }


    //при первой загрузки стены
    @Override
    public List<Article> getLastArticle() {
        Pageable pageable = PageRequest.of(0, SIZE_LIMIT, Sort.Direction.DESC, "id");
        return articleRepository.getLastArticle(pageable);
    }


    //при прокрутке
    @Override
    public List<Article> getArticleOffset(long idOffset) {
        Pageable pageable = PageRequest.of(0, SIZE_LIMIT, Sort.Direction.DESC, "id");
        return articleRepository.getOffsetArticle(idOffset, pageable);
    }

}
