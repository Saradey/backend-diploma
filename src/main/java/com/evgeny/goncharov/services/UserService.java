package com.evgeny.goncharov.services;

import com.evgeny.goncharov.entities.User;

import java.util.Optional;

public interface UserService {

    //регистрация, сохранения в db
    Boolean saveAndFlush(String pas, String login, String mail);

    //для теста
    User getById(String id);

    //чекам логин пароль
    Boolean checkLoginPassword(String login, String password);

}
