package com.evgeny.goncharov.services;

import com.evgeny.goncharov.entities.User;
import com.evgeny.goncharov.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;


    //регистрация
    @Override
    public Boolean saveAndFlush(String pas, String login, String mail) {
        if(checkUser(login)){
            return false;
        }
        else {
            User user = new User();
            user.setMail(mail);
            user.setPassword(pas);
            user.setLogin(login);
            userRepository.saveAndFlush(user);
            return true;
        }
    }



    private Boolean checkUser(String userId) {
        Optional<User> user =  userRepository.findById(userId);
        return user.isPresent();
    }


    @Override
    public Boolean checkLoginPassword(String login, String password){
        return userRepository.findByLoginAndPassword(login, password).isPresent();
    }


    @Override
    public User getById(String id) {
        User user = userRepository.findById(id).get();
        return user;
    }


}
